package com.example.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.model.User;
import com.example.myapplication.realm.RealmController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class Login_user extends AppCompatActivity {

    @BindView(R.id.input_number)
    EditText Number;

    @BindView(R.id.input_password)
    EditText Password;

    @BindView(R.id.btn_login)
    Button Login;

    private Realm realm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        this.realm = RealmController.with(this).getRealm();

    }

    @OnClick({R.id.btn_signup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                String number = Number.getText().toString();
                String password = Password.getText().toString();
                if ((!Number.equals(null)) || (!Password.equals(null))){
                    User user = new User();
                    user.setNumber(number);
                    user.setPassword(password);
                    realm.beginTransaction();
                    realm.copyToRealm(user);
                    realm.commitTransaction();

                }


        }
    }

}
