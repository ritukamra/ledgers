package com.example.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.example.myapplication.R;
import com.example.myapplication.model.User;
import com.example.myapplication.realm.RealmController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class Register_user extends AppCompatActivity {

    @BindView(R.id.input_name)
    EditText name;
    @BindView(R.id.input_mobile)
    EditText mobile;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.input_email)
    EditText email;
    private Realm realm;
    @BindView(R.id.btn_signup)
    Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        ButterKnife.bind(this);

        this.realm = RealmController.with(this).getRealm();

    }

    @OnClick({R.id.btn_signup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                String Name = name.getText().toString();
                String  Email = email.getText().toString();
                String Mobile = mobile.getText().toString();
                String Password = password.getText().toString();
                if ((!Name.equals(null)) || (!Email.equals(null)) || (!Mobile.equals(null)) || (!Password.equals(null))){
                User user = new User();
                user.setName(Name);
                user.setEmail(Email);
                user.setNumber(Mobile);
                user.setNumber(Password);
                realm.beginTransaction();
                realm.copyToRealm(user);
                realm.commitTransaction();

        }


    }
}
}