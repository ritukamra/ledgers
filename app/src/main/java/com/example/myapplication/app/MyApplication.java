package com.example.myapplication.app;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;




/**
 * Created by dinesh3836 on 29/01/17.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("smartValuer.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
    }
}