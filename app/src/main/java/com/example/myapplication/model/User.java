package com.example.myapplication.model;

import java.io.Serializable;

import io.realm.Realm;
import io.realm.RealmObject;

public class User extends RealmObject implements Serializable {

    private String name;
    private String email;
    private String password;
    private  String number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
